package com.forjino.test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.forjino.utility.Optional;

class OptionalTest {

	@Test
	void of() {
		Apple a = new Apple();
		
		if (Optional.of(a) instanceof Optional<?> == false) {
			fail("creating Optional from an Object must succeed");
		}
		
		try {
			Optional.of(null);
			fail("creating Optional from null with of must throw a NullPointerException");
		} catch (NullPointerException e) {
			// OK
		} catch (Exception e) {
			fail("creating Optional from null with of must throw a NullPointerException");
		}
	}
	
	@Test
	void ofNullable() {
		Apple a = new Apple();
		
		if (Optional.ofNullable(a) instanceof Optional<?> == false) {
			fail("creating Optional from an Object must succeed");
		}
		
		if (Optional.ofNullable(null) instanceof Optional<?> == false) {
			fail("creating Optional from byll with ofNullable must succeed");
		}
	}
	
	@Test
	void get() {
		Apple a = new Apple();
		
		Optional<Apple> nonNull = Optional.of(a);
		Optional<Apple> empty = Optional.empty();
		
		Apple b = nonNull.get();
		
		assertEquals(a, b);
		
		try {
			empty.get();
			fail("Trying to get the Object from an empty Optional must throw a NullPointerException");
		} catch (NullPointerException e) {
			// OK
		} catch (Exception e) {
			fail("Trying to get the Object from an empty Optional must throw a NullPointerException");
		}
	}
	
	@Test
	void isPresent() {
		Apple a = new Apple();
		
		Optional<Apple> nonEmpty = Optional.of(a);
		Optional<Apple> empty = Optional.empty();
		
		if (nonEmpty.isPresent() == false) {
			fail("isPresent must return true for a non empty Optional");
		}
		
		if (empty.isPresent() == true) {
			fail("isPresent must return false for an empty Optional");
		}
	}
	
	@Test
	void empty() {
		Optional<Apple> a = Optional.empty();
		
		if (a.isPresent() == true) {
			fail("Creating an empty Optional must return false for isPresent");
		}
	}
	
	@Test
	void orElse() {
		Apple a = new Apple();
		Apple b = new Apple();
		
		assertNotEquals(a, b);
		
		Optional<Apple> nonEmpty = Optional.of(a);
		Optional<Apple> empty = Optional.empty();
		
		Apple c = nonEmpty.orElse(b);
		Apple d = empty.orElse(b);
		
		assertEquals(a, c);		
		assertEquals(b, d);
	}
	
	@Test
	void iterator() {
		Apple a = new Apple();
		
		Optional<Apple> nonEmpty = Optional.of(a);
		Optional<Apple> empty = Optional.empty();
		
		int i = 0;
		
		for (Apple b : nonEmpty) {
			i++;
			assertEquals(a, b);
		}
		
		assertEquals(i, 1);
		
		for (Apple b : empty) {
			fail("Iterating over an empty Optional must not do anything! " + b);
		}
	}
	
	@Test
	void map() {
		Apple a = new Apple();
		Pear p = new Pear();
		
		Optional<Apple> nonEmpty = Optional.of(a);
		Optional<Apple> empty = Optional.empty();
		
		Optional<Pear> c = nonEmpty.map(x -> p);
		
		assertEquals(p, c.get());
		
		Optional<Pear> d = empty.map(x -> p);
		
		assertFalse(d.isPresent());
	}
	
	@Test
	void equalsAndhashCode() {
		// TODO
	}
	
	private class Apple {
		
	}
	
	private class Pear {
		
	}
}
