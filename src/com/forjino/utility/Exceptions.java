package com.forjino.utility;

import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class Exceptions {
	public static String stackTraceToString(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
