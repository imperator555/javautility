package com.forjino.utility;

import java.util.concurrent.TimeUnit;

public class ElapsedTimer {
	private long timeStarted = 0;

	public static ElapsedTimer createAndStart() {
		ElapsedTimer t = new ElapsedTimer();
		t.start();
		
		return t;
	}
	
	public void start() {
		timeStarted = System.nanoTime();
	}

	public long getElapsedTime() {
		if (timeStarted == 0) {
			return 0;
		}
		
		long elapsed = System.nanoTime() - timeStarted;
		return TimeUnit.NANOSECONDS.toMillis(elapsed);
	}
}

