package com.forjino.utility;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;

final public class Optional<T> implements Iterable<T> {
	private T innerObject;
	
	private Optional(T o) {
		innerObject = o;
	}
	
	public static <T> Optional<T> empty() {
		return new Optional<T>(null);
	}
	
	public static <T> Optional<T> of(T value) {
		if (value == null) {
			throw new NullPointerException();
		}
		
		return new Optional<T>(value);
	}
	
	public static <T> Optional<T> ofNullable(T value) {
		return new Optional<T>(value);
	}
	
	public T get() {
		if (innerObject == null) {
			throw new NullPointerException();
		}
		
		return innerObject;
	}
	
	public boolean isPresent() {
		return innerObject != null;
	}
	
	public T orElse(T other) {
		return innerObject != null ? innerObject : other;
	}
	
	public <U> Optional<U> map(Function<? super T, ? extends U> mapper) {
		if (innerObject != null) {
			return ofNullable(mapper.apply(innerObject));
		}
		
		return empty();
	}

	@Override
	public Iterator<T> iterator() {
		if (innerObject == null) {
			return Collections.emptyListIterator();
		} else {
			return Arrays.asList(innerObject).iterator();
		}
	}
	
	@Override
	public int hashCode() {
		return innerObject != null ? innerObject.hashCode() : 0;
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		
		return this.hashCode() == other.hashCode();
	}
	
	@Override
	public String toString() {
		return String.format("Optional[%s]", innerObject);
	}
}
