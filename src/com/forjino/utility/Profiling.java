package com.forjino.utility;

public abstract class Profiling {
	public static long getMemoryUsage() {
		long bytes = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		long megaBytes = bytes / 1024 / 1024;
		return megaBytes;
	}
}
